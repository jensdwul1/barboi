// pages/about.js

import Layout from '../components/Layout.js';

const indexPageContent = <p>Hello Next.js</p>;

export default function Index() {
    return <Layout content={indexPageContent} />;
}