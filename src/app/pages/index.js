// pages/index.js

import Layout from '../components/Layout.js';
import Link from 'next/link';
import fetch from 'isomorphic-unfetch';

const RoomLink = ({ room }) => (
    <li key={room.id}>
        <Link as={`/r/${room.id}`} href={`/room?id=${room.id}`}>
            <a>{room.name}</a>
        </Link>
    </li>
);

const Index = props => (
    <Layout>
        <h1>Batman TV Shows</h1>
        <ul>
            {props.shows.map(show => (
                <RoomLink key={show.id} room={show}></RoomLink>
            ))}
        </ul>
        <style jsx>{`
        h1,
        a {
          font-family: 'Arial';
        }
        ul {
          padding: 0;
        }
      `}</style>
    </Layout>
);
Index.getInitialProps = async function () {
    const res = await fetch('https://api.tvmaze.com/search/shows?q=batman');
    const data = await res.json();

    console.log(`Show data fetched. Count: ${data.length}`);

    return {
        shows: data.map(entry => entry.show)
    };
};

export default Index;