import Link from 'next/link';

const Header = () => (
    <aside className="bar-menu">
        <div className="bar-menu-content">

        </div>
        <div className="bar-menu-divider"></div>
        <div className="bar-menu-footer">

        </div>
        <style jsx>{`
            .bar-header {
                background: #333333;
                width: 100%;
                height: 2.5rem;
            }
        `}</style>
    </aside>
);

export default Header;