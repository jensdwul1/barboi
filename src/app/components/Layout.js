import Head from 'next/head';
import Header from './Header';
import Sidebar from './Sidebar';
import css from "../styles/scss/main.scss"

const layoutStyle = {
};

const Layout = props => (
    <div className="bar" style={layoutStyle}>
        <Head>
            <meta charSet="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
            <title>Bar Boi{}</title>
        </Head>
        <style jsx global>{`
        *, *:before, *:after {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }
        body { 
            font-family: sans-serif;
            line-height: 1.15;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            color: #fafafa;
            font: normal 125%/1.4 "Open Sans", "Helvetica Neue Light", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
            background: #1e1e1e;
            height: 100%;
            margin: 0;
            padding: 0;
        }
        
        html,
        main,
        #__next {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        `}</style>
        <Header />
        <div className="bar-content">
            <Sidebar></Sidebar>
            <main className="bar-canvas">{props.children}</main>
        </div>
    </div>
);

export default Layout;