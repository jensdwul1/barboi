import Link from 'next/link';
const linkStyle = {
    marginRight: 15
};

const Header = () => (
    <header className="bar-header">
        <div className="bar-menu-sync"></div>
        <Link href="/">
            <a style={linkStyle}>Home</a>
        </Link>
        <Link href="/about">
            <a style={linkStyle}>About</a>
        </Link>
    </header>
);

export default Header;